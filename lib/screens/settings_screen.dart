import 'package:flutter/material.dart';
import 'package:lucy_in_the_sky_test/providers/currency_provider.dart';
import 'package:lucy_in_the_sky_test/screens/settings_screen_mixin.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> with SettingsMixin {
  @override
  void initState() {
    super.initState();
    currencyProvider = Provider.of<CurrencyProvider>(context, listen: false);
    tempSelectedCurrencies = List.from(currencyProvider.displayedCurrencies);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Currency Settings'),
          actions: [
            IconButton(
              onPressed: () => saveSettings(context),
              icon: const Icon(Icons.save),
            )
          ],
        ),
        body: ReorderableListView(
          onReorder: reorderCurrencies,
          children: tempSelectedCurrencies.map((currency) {
            return Column(
              key: ValueKey(currency.code),
              children: [
                ListTile(
                  title: Text(currency.code),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Switch(
                        value: currency.visible,
                        onChanged: (_) => toggleVisibility(currency),
                      ),
                      const SizedBox(width: 12.0),
                      const Icon(Icons.drag_handle_rounded)
                    ],
                  ),
                ),
                const Divider(height: .0),
              ],
            );
          }).toList(),
        ),
      );
}
