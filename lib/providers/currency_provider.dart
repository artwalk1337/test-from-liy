import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:lucy_in_the_sky_test/models/currency.dart';
import 'package:lucy_in_the_sky_test/services/currency_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CurrencyProvider with ChangeNotifier {
  static const _sharedPrefKey = 'selectedCurrencies';

  List<Currency> _currencies = [];
  List<Currency> get currencies => _currencies;

  List<Currency> _displayedCurrencies = [];
  List<Currency> get displayedCurrencies => _displayedCurrencies;

  @override
  void dispose() {
    _currencies.clear();
    _displayedCurrencies.clear();
    super.dispose();
  }

  Future<void> initialize() async {
    await fetchCurrencies();
    await loadSettings();
  }

  @visibleForTesting
  Future<void> fetchCurrencies() async {
    try {
      final todayRates = await CurrencyService.fetchTodayRates();
      final yesterdayRates = await CurrencyService.fetchYesterdayRates();
      final fullRateList = <Currency>[];
      for (final i in todayRates) {
        final fullRate = i.copyWith(
            rateYesterday: yesterdayRates
                .firstWhere((e) => e.code == i.code)
                .rateYesterday);
        fullRateList.add(fullRate);
      }
      _currencies = fullRateList;
    } catch (_) {
      throw Error();
    }
  }

  @visibleForTesting
  Future<void> loadSettings() async {
    final prefs = await SharedPreferences.getInstance();
    final selectedCurrenciesJson = prefs.getString(_sharedPrefKey);
    final predefinedList = ['USD', 'EUR', 'RUB'];

    if (selectedCurrenciesJson == null) {
      for (final currency in _currencies) {
        if (predefinedList.contains(currency.code)) {
          currency.visible = true;
        }
      }
      _displayedCurrencies = _currencies;
      notifyListeners();
      return;
    }
    final selectedCurrenciesData = jsonDecode(selectedCurrenciesJson);
    _displayedCurrencies = selectedCurrenciesData
        .map<Currency>((currencyJson) => Currency.fromJson(currencyJson))
        .toList();
    notifyListeners();
  }

  Future<void> saveSettings(List<Currency> updatedCurrencies) async {
    final prefs = await SharedPreferences.getInstance();

    final currencyData =
        updatedCurrencies.map((currency) => currency.toJson()).toList();
    await prefs.setString('selectedCurrencies', json.encode(currencyData));

    _displayedCurrencies = List.from(updatedCurrencies);
    _currencies = List.from(updatedCurrencies);

    notifyListeners();
  }
}
