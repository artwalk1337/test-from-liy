import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:lucy_in_the_sky_test/models/currency.dart';

class CurrencyService {
  static const String _apiKey = '9bfb95659947458d87c739c7982ed9ac';

  static Future<List<Currency>> fetchTodayRates() async {
    final url = Uri.https('openexchangerates.org', '/api/latest.json', {
      'app_id': _apiKey,
      'base': 'USD',// For using any other base currencies u must pay for upgrading API plan
    });

    final response = await http.get(url);

    if (response.statusCode == 200) {
      final responseData = json.decode(response.body);

      if (responseData != null && responseData['rates'] != null) {
        final Map<String, dynamic> rates = responseData['rates'];

        final currencies = rates.entries.map((entry) {
          final code = entry.key;
          final rate = double.parse(entry.value.toString());

          return Currency(code: code, rateToday: rate, rateYesterday: .0);
        }).toList();

        return currencies;
      } else {
        throw Exception('Invalid response data');
      }
    } else {
      throw Exception('Failed to fetch currencies');
    }
  }

  static Future<List<Currency>> fetchYesterdayRates() async {
    final yesterdayDate = DateFormat('yyyy-MM-dd')
        .format(DateTime.now().subtract(const Duration(days: 1)));

    final url = Uri.https(
        'openexchangerates.org', '/api/historical/$yesterdayDate.json', {
      'app_id': _apiKey,
      'base': 'USD',// For using any other base currencies u must pay for upgrading API plan
    });

    final response = await http.get(url);

    if (response.statusCode == 200) {
      final responseData = json.decode(response.body);

      if (responseData != null && responseData['rates'] != null) {
        final Map<String, dynamic> rates = responseData['rates'];

        final currencies = rates.entries.map((entry) {
          final code = entry.key;
          final rate = double.parse(entry.value.toString());

          return Currency(code: code, rateToday: .0, rateYesterday: rate);
        }).toList();

        return currencies;
      } else {
        throw Exception('Invalid response data');
      }
    } else {
      throw Exception('Failed to fetch currencies');
    }
  }
}
