import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'providers/currency_provider.dart';
import 'screens/home_screen.dart';
import 'screens/settings_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) => FutureBuilder(
        future: _initializeApp(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return MaterialApp(
              home: Scaffold(
                appBar: AppBar(title: const Text('Currency Exchange')),
                body: const Center(child: CircularProgressIndicator()),
              ),
            );
          } else if (snapshot.hasError) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              home: Scaffold(
                appBar: AppBar(title: const Text('Currency Exchange')),
                body: const Center(child: Text('Error fetching rates')),
              ),
            );
          } else {
            return MultiProvider(
              providers: [
                ChangeNotifierProvider(
                    create: (_) => snapshot.data as CurrencyProvider),
              ],
              child: MaterialApp(
                title: 'Currency App',
                theme: ThemeData(primarySwatch: Colors.blue),
                initialRoute: '/',
                routes: {
                  '/': (ctx) => const HomeScreen(),
                  '/settings': (ctx) => const SettingsScreen(),
                },
              ),
            );
          }
        },
      );

  Future<CurrencyProvider> _initializeApp() async {
    final currencyProvider = CurrencyProvider();
    await currencyProvider.initialize();
    return currencyProvider;
  }
}
