import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:lucy_in_the_sky_test/models/currency.dart';
import 'package:lucy_in_the_sky_test/providers/currency_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('CurrencyProvider Tests', () {
    late CurrencyProvider currencyProvider;

    setUpAll(() async {
      HttpOverrides.global = null;
      SharedPreferences.setMockInitialValues({});
      currencyProvider = CurrencyProvider();
      await currencyProvider.initialize();
    });

    test('Fetch Currencies', () async {
      await currencyProvider.fetchCurrencies();
      expect(currencyProvider.currencies.isNotEmpty, true);
    });

    test('Load Settings - Default', () async {
      await currencyProvider.loadSettings();
      expect(
        currencyProvider.displayedCurrencies.length,
        currencyProvider.currencies.length,
      );
    });

    test('Save Settings', () async {
      final updatedCurrencies = [
        Currency(
            code: 'USD', rateToday: 1.0, rateYesterday: 1.0, visible: false),
        Currency(
            code: 'EUR', rateToday: 1.0, rateYesterday: 1.0, visible: true),
        Currency(
            code: 'RUB', rateToday: 1.0, rateYesterday: 1.0, visible: true),
      ];

      await currencyProvider.saveSettings(updatedCurrencies);

      final prefs = await SharedPreferences.getInstance();
      final selectedCurrenciesJson = prefs.getString('selectedCurrencies');
      final selectedCurrenciesData = jsonDecode(selectedCurrenciesJson!);
      final selectedCurrencies = selectedCurrenciesData
          .map<Currency>((currencyJson) => Currency.fromJson(currencyJson))
          .toList();

      expect(selectedCurrencies.length, 3);
      expect(selectedCurrencies[0].visible, false);
      expect(selectedCurrencies[1].visible, true);
      expect(selectedCurrencies[2].visible, true);
    });
  });
}
