class Currency {
  final String code;
  final double rateToday;
  final double rateYesterday;
  bool visible;

  Currency({
    required this.code,
    required this.rateToday,
    required this.rateYesterday,
    this.visible = false,
  });

  Currency copyWith({
    String? code,
    double? rateToday,
    double? rateYesterday,
    bool? visible,
  }) {
    return Currency(
      code: code ?? this.code,
      rateToday: rateToday ?? this.rateToday,
      rateYesterday: rateYesterday ?? this.rateYesterday,
      visible: visible ?? this.visible,
    );
  }

  factory Currency.fromJson(Map<String, dynamic> json) {
    return Currency(
      code: json['code'],
      rateToday: json['rateToday'],
      rateYesterday: json['rateYesterday'],
      visible: json['visible'] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'code': code,
      'rateToday': rateToday,
      'rateYesterday': rateYesterday,
      'visible': visible,
    };
  }

  @override
  String toString() {
    return toJson().toString();
  }
}
