import 'package:flutter/material.dart';
import 'package:lucy_in_the_sky_test/models/currency.dart';
import 'package:lucy_in_the_sky_test/providers/currency_provider.dart';
import 'package:lucy_in_the_sky_test/screens/settings_screen.dart';

mixin SettingsMixin on State<SettingsScreen> {
  late final List<Currency> tempSelectedCurrencies;
  late final CurrencyProvider currencyProvider;

  void reorderCurrencies(int oldIndex, int newIndex) => setState(() {
        if (oldIndex < newIndex) {
          newIndex -= 1;
        }
        final currency = tempSelectedCurrencies.removeAt(oldIndex);
        tempSelectedCurrencies.insert(newIndex, currency);
      });

  void toggleVisibility(Currency currency) => setState(() {
        final index = tempSelectedCurrencies.indexOf(currency);
        tempSelectedCurrencies[index] =
            currency.copyWith(visible: !currency.visible);
      });

  void saveSettings(BuildContext context) {
    currencyProvider.saveSettings(tempSelectedCurrencies);
    Navigator.pop(context);
  }
}
