import 'package:flutter/material.dart';
import 'package:lucy_in_the_sky_test/providers/currency_provider.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final currencyProvider = Provider.of<CurrencyProvider>(context);
    final selectedCurrencies = currencyProvider.displayedCurrencies
        .where((currency) => currency.visible)
        .toList();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Currency Exchange'),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.pushNamed(context, '/settings');
            },
          ),
        ],
      ),
      body: ListView.separated(
        itemCount: selectedCurrencies.length,
        itemBuilder: (ctx, index) {
          final currency = selectedCurrencies[index];
          return ListTile(
            title: Text(currency.code),
            subtitle: Text(
              'YDA: ${currency.rateYesterday.toStringAsFixed(2)}',
              style: const TextStyle(fontSize: 12.0),
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text('Today: '),
                Text(
                  currency.rateToday.toStringAsFixed(2),
                  style: const TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          );
        },
        separatorBuilder: (_, __) => const Divider(height: .0),
      ),
    );
  }
}
